var secret = "hello"

module.exports = {
	name: "IamClass",
	lower: function(input) {
		return input.toLowerCase();
	},
	get_name: function() {
		return this.name;
	},
	get_secret: function() {
		return secret
	}
};
